<!DOCTYPE html>
<html>
<head>
	{{ Html::style('css/bootstrap.min.css') }}
	{{ Html::style('css/style.css') }}
	<title>@yield('title')</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row header">
			<div class="col-lg-12">
				<h1><a href="/">GSB</a></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 panel">
				<ul>
					<li><a href="/"> 1</a></li>
					<li><a href="/">1</a></li>
				</ul>
			</div>

			<div class="col-lg-10 body">
				@yield('content')
			</div>
		</div>
	</div>


	




<footer>
	{{ Html::script('js/bootstrap.min.js') }}
	{{ Html::script('js/jquery.min.js') }}
	{{ Html::script('js/script.js') }}
</footer>
</body>
</html>
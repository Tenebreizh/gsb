<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
    return view('index');
});


Route::get('/visiteur/create', 'VisiteurController@showCreate');

Route::get('/visiteur/fiche/{id}', 'VisiteurController@fiche');

Route::get('/visiteur/liste', 'VisiteurController@liste');



Route::get('/frais/create', 'FicheFraisController@method');


<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichefraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichefrais', function(Blueprint $table)
        {
            $table->increments('id');
            $table->char('idVisiteur', 4);
            $table->integer('nbJustificatifs')->nullable();
            $table->decimal('montantValide', 10, 2)->nullable();
            $table->string('dateModif', 7)->nullable();
            $table->char('idEtat', 2)->default('CR')->nullable();
            $table->string('date', 7) ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fichefrais');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisiteurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visiteur', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nom',30);
            $table->string('prenom', 30);
            $table->string('adresse', 30)->nullable();
            $table->string('cp', 5)->nullable();
            $table->string('ville', 30)->nullable();
            $table->date('dateEmbauche')->nullable();
            $table->string('login', 60);
            $table->string('pwd', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visiteur');
    }
}
